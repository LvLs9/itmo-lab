from django.db import models


class Mode(models.Model):
    name = models.CharField(max_length=512)


class WorkerState(models.Model):
    name = models.CharField(max_length=512)


class Education(models.Model):
    name = models.CharField(max_length=512)

    def __str__(self):
        return self.name


class Worker(models.Model):
    FEMALE = 'F'
    MALE = 'M'
    SEX_CHOICES = [
        (FEMALE, 'female'),
        (MALE, 'male'),
    ]

    name = models.CharField(max_length=512)
    birthday = models.DateField()
    sex = models.CharField(max_length=8, choices=SEX_CHOICES, default=MALE)
    education = models.ForeignKey(Education, on_delete=models.PROTECT)
    experience_months = models.IntegerField(default=0)
    employment_record_number = models.CharField(max_length=64)
    is_super = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.id}/{self.birthday}: {self.name}'


class Departament(models.Model):
    name = models.CharField(max_length=128)
    phone_number = models.CharField(max_length=64)

    def __str__(self):
        return f'{self.name}: {self.phone_number}'


class EmploymentContract(models.Model):
    departament = models.ForeignKey(Departament, on_delete=models.CASCADE)
    worker = models.ForeignKey(Worker, on_delete=models.CASCADE)
    position = models.CharField(max_length=128)
    salary = models.DecimalField(max_digits=32, decimal_places=2)
    start = models.DateTimeField()
    end = models.DateTimeField()
    is_head = models.BooleanField(default=False)


class EducationCenter(models.Model):
    name = models.CharField(max_length=512)

    def __str__(self):
        return self.name


class Training(models.Model):
    worker = models.ForeignKey(Worker, on_delete=models.PROTECT)
    program = models.CharField(max_length=256)
    education_center = models.ForeignKey(EducationCenter, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.education_center}: {self.program}'
