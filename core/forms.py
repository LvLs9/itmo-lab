from django import forms

from core.models import Departament, Worker, EmploymentContract, Training


class LoginForm(forms.Form):
    id = forms.CharField(label='Your ID', max_length=64)
    birthday = forms.CharField(label='Birthday in YYYY-MM-DD')


class DepartamentForm(forms.ModelForm):
    class Meta:
        model = Departament
        fields = ['name', 'phone_number']


class DepartamentWorkerAdderForm(forms.ModelForm):
    worker = forms.ModelMultipleChoiceField(queryset=Worker.objects.none())

    def __init__(self, *args, workers=None, **kwargs):
        super().__init__(*args, **kwargs)
        if not workers:
            workers = []
        self.fields['worker'].queryset = Worker.objects.exclude(id__in=[worker.id for worker in workers])

    class Meta:
        model = EmploymentContract
        fields = [
            'position',
            'salary',
            'start',
            'end',
            'is_head',
        ]


class WorkerForm(forms.ModelForm):
    class Meta:
        model = Worker
        fields = [
            'name',
            'birthday',
            'sex',
            'education',
            'experience_months',
            'employment_record_number',
        ]


class TrainingForm(forms.ModelForm):
    class Meta:
        model = Training
        fields = ['program', 'education_center']
