from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('create-departament/', views.create_departament, name='create-departament'),
    path('delete-departament/<departament_id>', views.delete_departament, name='delete-departament'),
    path('update-departament/<departament_id>', views.update_departament, name='update-departament'),
    path('add-departament-worker/<departament_id>', views.add_departament_worker, name='add-departament-worker'),
    path('delete-departament-worker/<departament_id>/<worker_id>', views.delete_departament_worker,
         name='delete-departament-worker'),
    path('create-worker/', views.create_worker, name='create-worker'),
    path('delete-worker/<worker_id>', views.delete_worker, name='delete-worker'),
    path('update-worker/<worker_id>', views.update_worker, name='update-worker'),
    path('add-training/<worker_id>', views.add_training, name='add-training'),
    path('delete-training/<training_id>', views.delete_training, name='delete-training'),
    path('logout/', views.logout, name='logout'),
]
